@extends('layouts.app')

@section('title')
    Dentist Service
@endsection

@section('content')
    <div class="page-content page-services">
      <section
        class="store-breadcrumbs"
        data-aos="fade-down"
        data-aos-delay="100"
      >
        <div class="container">
          <div class="row">
            <div class="col-12">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">
                    Services
                  </li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </section>
      <section class="dentist-greeting">
        <div class="container text-center">
          <div class="row">
            <div class="col-12" data-aos="fade-up">
              <h4>Service</h4>
              <h2>Kami dari Dental Care drg. Sri Kurniati</h2>
            </div>
          </div>
        </div>
      </section>
      <section class="dentist-image-services">
        <div class="container">
          <div class="row">
            <div class="col-lg-12" data-aos="zoom-in">
              <img
                src="/images/image-about-us 1.png"
                class="d-block w-100 rounded"
                alt="Carousel Image"
              />
              <hr />
            </div>
          </div>
        </div>
      </section>
      <section class="dentist-serivce">
        <div class="container">
          <div class="row">
            <div
              class="col-6 col-md-3 col-lg-2"
              data-aos="fade-up"
              data-aos-delay="100"
            >
              <a class="component-service-categories d-block" href="#">
                <div class="categories-image">
                  <img
                    src="/images/categories-gadgets.svg"
                    alt="Gadgets Categories"
                    class="w-100"
                  />
                </div>
                <p class="categories-text">Perawatan Umum</p>
              </a>
            </div>
            <div
              class="col-6 col-md-3 col-lg-2"
              data-aos="fade-up"
              data-aos-delay="200"
            >
              <a class="component-service-categories d-block" href="#">
                <div class="categories-image">
                  <img
                    src="/images/categories-furniture.svg"
                    alt="Furniture Categories"
                    class="w-100"
                  />
                </div>
                <p class="categories-text">Pembuatan Gigi Palsu</p>
              </a>
            </div>
            <div
              class="col-6 col-md-3 col-lg-2"
              data-aos="fade-up"
              data-aos-delay="300"
            >
              <a class="component-service-categories d-block" href="#">
                <div class="categories-image">
                  <img
                    src="/images/categories-makeup.svg"
                    alt="Makeup Categories"
                    class="w-100"
                  />
                </div>
                <p class="categories-text">Meratakan Gigi</p>
              </a>
            </div>
            <div
              class="col-6 col-md-3 col-lg-2"
              data-aos="fade-up"
              data-aos-delay="400"
            >
              <a class="component-service-categories d-block" href="#">
                <div class="categories-image">
                  <img
                    src="/images/categories-sneaker.svg"
                    alt="Sneaker Categories"
                    class="w-100"
                  />
                </div>
                <p class="categories-text">Perawatan Gigi Anak</p>
              </a>
            </div>
            <div
              class="col-6 col-md-3 col-lg-2"
              data-aos="fade-up"
              data-aos-delay="500"
            >
              <a class="component-service-categories d-block" href="#">
                <div class="categories-image">
                  <img
                    src="/images/categories-tools.svg"
                    alt="Tools Categories"
                    class="w-100"
                  />
                </div>
                <p class="categories-text">Veneer Gigi</p>
              </a>
            </div>
            <div
              class="col-6 col-md-3 col-lg-2"
              data-aos="fade-up"
              data-aos-delay="600"
            >
              <a class="component-service-categories d-block" href="#">
                <div class="categories-image">
                  <img
                    src="/images/categories-baby.svg"
                    alt="Baby Categories"
                    class="w-100"
                  />
                </div>
                <p class="categories-text">Perawatan Akar</p>
              </a>
            </div>
          </div>
        </div>
      </section>
    </div>
@endsection